@props(['name'])
<label for="inputFirstName" class="form-label">{{ ucwords(str_replace("_"," ",$name)) }}</label>
<input name="{{ $name }}" class="form-control" id="{{ $name }}" placeholder="{{ ucwords(str_replace("_"," ",$name)) }}" {{ $attributes }}>
@error($name)
    <span class="sm text-danger">{{ $message }}</span>
@enderror
