<x-backend.layout.master>

    <!-- Sidebar Start -->
    <x-backend.partials.sidebar />
    <!-- Sidebar End -->



    <!-- Content Start -->
    <div class="content">
        <!-- Navbar Start -->
        <x-backend.partials.navbar />
        <!-- Navbar End -->

        <!-- content Start -->

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('message'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>{{ session('message') }}!</strong>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <div class="container mt-4">
            <div class=" card card-body">
                <p>Patient Name: {{ $prescription->patient_name }} </p>
                <p>Doctor Name: {{ $prescription->doctor_name }} </p>
                <p>Doctor Category: {{ $prescription->doctor_category }} </p>
                <p>Medicine: {{ $prescription->medicine }} </p>
                <p>Age: {{ $prescription->age }} </p>
                <p>Phone Number: {{ $prescription->phone }} </p>
                <p>Test: {{ $prescription->test }} </p>
                <p>Disease: {{ $prescription->disease }} </p>
                <p>Gender: {{ $prescription->gender }} </p>
                <p>Next Appointment: {{ $prescription->next_appointment }} </p>
                <p>Message: {{ $prescription->message }} </p>
            </div>
        </div>


        <!-- content End -->


        <!-- Footer Start -->
        <x-backend.partials.footer />
        <!-- Footer End -->

    </div>
    <!-- Content End -->

    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>




    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>


</x-backend.layout.master>
