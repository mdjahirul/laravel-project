<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prescription extends Model
{
    use HasFactory;
    protected $fillable = [
        'patient_name',
        'doctor_name',
        'doctor_category',
        'medicine',
        'age',
        'phone',
        'test',
        'disease',
        'gender',
        'next_appointment',
        'messages'
    ];
}
