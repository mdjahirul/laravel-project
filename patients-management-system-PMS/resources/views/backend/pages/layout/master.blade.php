<x-backend.layout.master>

    <!-- Sidebar Start -->
    <x-backend.partials.sidebar/>
    <!-- Sidebar End -->
       


    <!-- Content Start -->
    <div class="content">
        <!-- Navbar Start -->
        <x-backend.partials.navbar/>
        <!-- Navbar End -->

        <!-- content Start -->
        <x-backend.partials.content/>

         <!-- content End -->


        <!-- Footer Start -->
        <x-backend.partials.footer/>
        <!-- Footer End -->

    </div>
    <!-- Content End -->

    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
</div>
       
      

       <!-- Back to Top -->
       <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
   </div>

</x-backend.layout.master>
