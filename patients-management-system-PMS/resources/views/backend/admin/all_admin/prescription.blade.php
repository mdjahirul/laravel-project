<x-backend.layout.master>

    <!-- Sidebar Start -->
    <x-backend.partials.sidebar />
    <!-- Sidebar End -->



    <!-- Content Start -->
    <div class="content">
        <!-- Navbar Start -->
        <x-backend.partials.navbar />
        <!-- Navbar End -->

        <!-- content Start -->
        <!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
  </head>
  <body>


 <div class="container mt-4">

  <form action="" class="bg-light rounded-3 border border-5 border-primary" >
  <div>
        <h3 class="text-uppercase text-center text-white bg-primary p-2 ">pascription form</h3>
        </div>
   <div class="p-4 ">
    
        <div class="row mb-3">
            
            <div class=" col-2 text-end">
                <label for="ductor name" class="p-1 mr-1 text-uppercase fw-bolder"> Physician-Name:</label>
            </div>
                <div class="col-4  ">
                    <input type="text" id="ductor name" placeholder="Physician-Name" name="ductor" class="form-control">
                </div>
                   

                <div class="col-3 text-end ">
                    <label for="ductor" class="p-1 mr-1 text-uppercase  fw-bolder"> Doctor-Category:</label>
                </div>
                <div class="col-3 ">
                    <select class="form-select" aria-label="multiple select example">
                    <option selected> Medicine Specialist</option>
                    <option value="1">Emergency physicians.</option>
                    <option value="2">Neurologists</option>
                    <option value="3">Pediatricians.</option>
                    </select>
                </div>

        </div>   

            

                <div class="row mt-4">

                        <div class="col-2 text-end">
                           <label for="Patient Name" class=" mt-1 text-uppercase  fw-bolder">Patient-Name:</label>
                        </div>

                        <div class="col-4">
                            <input type="text" name="Patient Name" id="Patient Name" class="form-control" placeholder="Enter Patient Name">
                        </div>

                        <div class="col-3 text-end">
                            
                            <label for="Phone Number" class="text-end mt-1 text-uppercase  fw-bolder">Patient Phone-Number:</label>
                        </div>

                        <div class=" col-3">
                            <input type="number" name="Phone Number" id="Phone Number" class="form-control" placeholder="Enter Phone Number">
                        </div>

                </div>  

                <div class="row mt-4">

                    <div class="col-2 text-end">
                        <label for="age" class=" p-1 text-uppercase  fw-bolder">Age:</label>   
                    </div>

                    <div class="col-2 ">
                      <input type="number" name="age" id="age" class="form-control" placeholder="Age">
                    </div>

                    <div class="col-1 text-end">
                        <label for="sex" class="p-1 text-uppercase  fw-bolder">Sex:</label>
                    </div>

                    <div class="col-2">
                        <select class="form-select" aria-label="multiple select example">
                        <option selected> Male</option>
                        <option value="1">Female</option>
                        <option value="2">Others</option>
                        </select>
                    </div>

                    <div class="col-2 text-end ">
                        <label for="age" class="p-1 text-uppercase  fw-bolder">Date Of Birth:</label>
                     </div>

                    <div class="col-3">
                        <input type="date" class="form-control">
                    </div>
                        
                </div>


                <div class="row mt-4">

                    <div class="col-2 text-end"> 
                        <label for="desiase" class="p-1 text-uppercase  fw-bolder" >desiase:</label>
                    </div>

                    <div class="col-4">
                        <input type="text" name="desiase" id="desiase" class="form-control" placeholder="desiase" >
                    </div>

                    <div class="col-2 text-end">
                        <label for="room" class="p-1 text-uppercase  fw-bolder" >Room No:</label>
                    </div>

                    <div class="col-4">
                        <select class="form-select" aria-label="multiple select example">
                            <option selected>401-BLock-D</option>
                            <option value="1">303-Block-f</option>
                            <option value="2">503-Block-e</option>
                        </select>
                    </div>
                </div>


                <div class="row mt-4" >

                    <div class="col-2"> </div>

                    <div class="col-2 text-end">
                        <label for="medicine" class="p-1 text-uppercase  fw-bolder">Medicine:</label>
                    </div>

                    <div class="col-5">
                        <select class="form-select" aria-label="multiple select example">
                            <option selected>Napa-10mg</option>
                            <option value="1">Finix-20mg</option>
                            <option value="2">Others</option>
                        </select>
                    </div>

                    <div class=" col-3"></div>
                </div>

                
                <div class="row mt-5">
                    <div class="col-3 text-end">
                        <label class="mt-3 text-uppercase  fw-bolder" for="message">Additional information:</label>
                    </div>

                    <div class="col-9">
                       <textarea class="form-control" name="message" id="message" rows="4"></textarea>
                    </div>
                
                </div>

                <div class="row mt-5">
                    <div class="col-5"></div>
                    <div class=" col-2  text-uppercase  fw-bolder btn btn-outline-primary text-center p-2 ">save</div>
                <div class="col-5"></div>

                </div>

           
    </div>
    </form>
 </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
  </body>
</html>
        <!-- content End -->


        <!-- Footer Start -->
        <x-backend.partials.footer />
        <!-- Footer End -->

    </div>
    <!-- Content End -->

    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>



    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>

</x-backend.layout.master>
