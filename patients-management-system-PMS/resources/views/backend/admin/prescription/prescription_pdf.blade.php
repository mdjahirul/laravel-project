


    <table>
        <thead class="table-dark">
            <tr class="text-center">
                <th scope="col">Ser No</th>
                <th scope="col"> Patient_Name</th>
                <th scope="col">Doctor_Name</th>
                <th scope="col">Doctor_Category</th>
                <th scope="col">Medicine</th>
                <th scope="col">Age</th>
                <th scope="col">Phone</th>
                <th scope="col">Test</th> 
                <th scope="col">Disease</th>
                <th scope="col">Gender</th>
                <th scope="col">Next_appointment</th>
                <th scope="col">Message</th>
                
            </tr>
        </thead>
        <tbody>

           

            @foreach ($prescription as $key => $prescription)
                <tr class="text-center">
                    <td scope="row">{{ $key + 1 }}</td>
                    <td>{{ $prescription->patient_name }}</td>
                    <td>{{ $prescription->doctor_name }}</td>
                    <td>{{ $prescription->doctor_category }}</td>
                    <td>{{ $prescription->medicine }}</td>
                    <td>{{ $prescription->age }}</td>
                    <td>{{ $prescription->phone }}</td>
                    <td>{{ $prescription->test }}</td>
                    <td>{{ $prescription->disease }}</td>
                    <td>{{ $prescription->gender }}</td>
                    <td>{{ $prescription->next_appointment }}</td>
                    <td>{{ $prescription->message }}</td>
                    
                </tr>
            @endforeach
        </tbody>
    </table>

    <style>

        table, th , td {
            padding: 20px;
            border: 1px solid black;
            border-collapse: collapse;   
        }
    
        table{
            width:100%;
        }
    
    
    </style>
