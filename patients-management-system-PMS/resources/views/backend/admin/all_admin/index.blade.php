<x-backend.layout.master>

    <!-- Sidebar Start -->
    <x-backend.partials.sidebar />
    <!-- Sidebar End -->



    <!-- Content Start -->
    <div class="content">
        <!-- Navbar Start -->
        <x-backend.partials.navbar />
        <!-- Navbar End -->

        <!-- content Start -->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('message'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>{{ session('message') }}!</strong>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('success') }}!</strong> <button type="button" class="btn-close"
                    data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif


        <div class="container mt-5 mx-4">

            <a href="{{ route('admin.register.form') }}" class="btn btn-sm btn-primary mb-3 mt-0"> Add new</a>



            <div class="container">

                <form action="" method="GET" class="d-flex mb-5">

                    <input type="text" name="keyword" placeholder="Search Here " class="form-control" />

                    <button class="btn btn-sm btn-primary">Search </button>

                </form>

            </div>


            <table class="table table table-striped">
                <thead class="table-dark">
                    <tr class="text-center">
                        <th scope="col">Ser No</th>
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Doctor Category</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Role</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>



                    @php
                        
                    @endphp

                    @foreach ($admins as $key => $admin)
                        <tr class="text-center">
                            <td scope="row">{{ $key + 1 }}</td>
                            <td>{{ $admin->first_name }}</td>
                            <td>{{ $admin->last_name }}</td>
                            <td>{{ $admin->doctor_category }}</td>
                            <td>{{ $admin->email }}</td>
                            <td>{{ $admin->phone }}</td>
                            <td>{{ $admin->adminRole->name }}</td>
                            <td class="d-flex">
                                <div><a class="btn btn-sm btn-primary  border border-1 border border-white"
                                        href="{{ route('admin.show', $admin->id) }}">Show</a></div>

                                <div> <a class="btn btn-sm btn-warning  border border-1 border border-white"
                                        href="{{ route('admin.edit', $admin->id) }}">Edit</a></div>



                                <form action="{{ route('admin.destroy', $admin->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-danger  border border-1 border border-white"
                                        onclick="return confirm('Are You Sure Want To Delete?')">Delete</button>
                                </form>

                                {{-- <a class="btn btn-danger" href="{{ route('admin.destroy', $admin->id) }}">
                                    Delete</a> --}}

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <!-- content End -->


        <!-- Footer Start -->
        <x-backend.partials.footer />
        <!-- Footer End -->

    </div>
    <!-- Content End -->

    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>




    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>


</x-backend.layout.master>
