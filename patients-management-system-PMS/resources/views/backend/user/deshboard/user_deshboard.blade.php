<x-backend.layout.master>

    <!-- Sidebar Start -->
    <x-backend.partials.sidebar />
    <!-- Sidebar End -->



    <!-- Content Start -->
    <div class="content">
        <!-- Navbar Start -->
        <x-backend.partials.navbar />
        <!-- Navbar End -->

        <!-- content Start -->

        <div class="container">
            <div class="row mx-4">
                <h3 class="text-center mt-3">Doctor Specialist</h3>
                @foreach ($doctors as $doctor)
                    <div class="col col-3">
                        <div class="card" style="width: 15rem; margin:auto;">

                            <img class="card-img-top" src="{{ asset('images/'. auth()->user()->image) }}"
                          alt="" style="width: 40px; height: 40px;">
                            {{-- <img src="" class="card-img-top" alt="..."> --}}
                            <div class="card-body">
                                <h5 class="card-title">
                                    {{ Str::ucfirst($doctor->first_name) }}
                                </h5>
                                <p class="card-title">{{ $doctor->description }}</p>

                                <p class="card-text"></p>
                                    <div class="d-flex"><a href="" class="btn btn-success border border-1 border border-white ">Show</a>

                                        <div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu2" data-bs-toggle="dropdown" aria-expanded="false">
                                                Appointment
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                                <div class="d-flex">
                                                    <a class="btn btn-sm btn-success border border-1 border border-white" href="{{ route('appointment.create',["id"=>$doctor->id]) }} "> Virtual Appointment </a>
   
                                                    <a class="btn btn-sm btn-info border border-1 border border-white" href="{{ route('appointment.create',["id"=>$doctor->id]) }}"> Physical Appointment </a>
                                              
                                                </div>
                                              
                                            </div>
                                        </div>
                                        
                                    </div>

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <!-- content End -->


        <!-- Footer Start -->
        <x-backend.partials.footer />
        <!-- Footer End -->

    </div>
    <!-- Content End -->

    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>



    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>

</x-backend.layout.master>
