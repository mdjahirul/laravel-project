<?php

namespace App\Http\Controllers\backend\user;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Models\DoctorCategory;
use App\Http\Controllers\Controller;

class DoctorCategoryController extends Controller
{
    public function doctorList($id)
    {
        $specific_doctor_category_list = Admin::where('doctor_category',$id)->get();
        return view('backend.user.doctor_category.specific_doctor_category_list', compact('specific_doctor_category_list'));
    }

    public function doctorSpecific($id)
    {
        $doctor = Admin::where('id',$id)->first();
        dd($doctor);
        return view('backend.user.doctor_category.show', compact('doctor'));
    }
}
