<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Database\QueryException;

class ProductController extends Controller
{
    public function index()
    {
        try{
            $products = Product::latest()->get();
                return view('backend.products.index',[
                'products' => $products,
            ]);
            
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function create()
    {
        return view('backend.products.create');
    }

    public function store(Request $request)
    {
        try{
            Product::create([
                'name' => $request->name,
                'category_name' =>$request->category_name,
                'brand_name' => $request->brand_name,
                'description' => $request->description,
                'image' => request()->hasFile('image') ? $this->uploadImage(request()->file('image')) : null,
            ]);
            return redirect()->route('products.index')->withMessage('Task was successful Created!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show(Product $product)
    {
        return view('backend.products.show',[
            'product' => $product
        ]);
    }

    public function edit(Product $product)
    {
        return view('backend.products.edit',[
            'product' =>$product
        ]);
    }

    public function update(Request $request, Product $product)
    {
        try{
            $product->update([
                'name' => $request->name,
                'category_name' =>$request->category_name,
                'brand_name' => $request->brand_name,
                'description' => $request->description,
                'image' => request()->hasFile('image') ? $this->uploadImage(request()->file('image')) : null,
            ]);
            return redirect()->route('products.index')->withMessage('Task was successfully Update!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy(Product $product)
    {
        try{
            $product->delete();
            return redirect()->route('products.index')->withMessage('Task was successfully Delete!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function uploadImage($file, $name=null)
    {
        $fileName = $name.time().'.'.$file->getClientOriginalExtension();
        Image::make($file)->resize(100, 100)->save(storage_path().'/app/public/images/products/'.$fileName);
        return $fileName;
        
    }
}
