<?php 
namespace jahirul;
use PDO;
use PDOException;

class DB{
    public $pdo;
    public function connection(){
        $hostname = "localhost";
        $username = "root";
        $password = "";
        $dbname = "php_crud";

        try {
            $this->pdo= new PDO(
                "mysql:host=$hostname; dbname=$dbname",
                $username,
                $password,
              );
              $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

        } catch (PDOException $e) {
            echo $e->getMessage(). $e->getLine();
        }
    }

    public function ready($sql){
      return  $this->pdo->prepare($sql);
    }
}
// $data= new DB;
// $data->connection();
