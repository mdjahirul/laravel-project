<x-backend.layout.master>

    <!-- Sidebar Start -->
    <x-backend.partials.sidebar />
    <!-- Sidebar End -->



    <!-- Content Start -->
    <div class="content">
        <!-- Navbar Start -->
        <x-backend.partials.navbar />
        <!-- Navbar End -->

        <!-- content Start -->



        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('message'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>{{ session('message') }}!</strong>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <div class="container mt-4">

            <form action="{{ route('admin.prescription.store') }}" method="POST"
                class="bg-light rounded-3 border border-5 border-primary">


                @csrf

                <div>
                    <h3 class="text-uppercase text-center text-white bg-primary p-2 ">pascription form</h3>
                </div>
                <div class="p-4 ">

                    <input type="hidden" id="ductor_name" placeholder="Physician-Name" name="doctor_name"
                        class="form-control" value="{{ auth('admin')->user()->first_name }}">

                    <input type="hidden" name="doctor_category" id=" doctor_category" class="form-control"
                        value="{{ auth('admin')->user()->doctor_category }}">



                    <div class="row mb-4">
                        <div class="col-8"></div>
                        <div class="col-1 text-end">
                            <label for="date" class="mt-1 text-uppercase fw-bolder">date:</label>
                        </div>

                        <div class="col-3">
                            <input type="date" name="date" id="date" class="form-control">
                        </div>

                    </div>


                    <div class="row mt-4">

                        <div class="col-2 text-end">
                            <label for="Patient Name" class=" mt-1 text-uppercase  fw-bolder">Patient-Name:</label>
                        </div>

                        <div class="col-4">
                            <input type="text" name="patient_name" id="Patient Name" class="form-control"
                                placeholder="Enter Patient Name" value={{ $patient->name }}>
                        </div>

                        <div class="col-3 text-end">

                            <label for="Phone Number" class="text-end mt-1 text-uppercase  fw-bolder">Patient
                                Phone-Number:</label>
                        </div>

                        <div class=" col-3">
                            <input type="number" name="phone" id="Phone Number" class="form-control"
                                placeholder="Enter Phone Number" value={{ $patient->phone }}>
                        </div>

                    </div>

                    <div class="row mt-4">

                        <div class="col-2 text-end mt-1">
                            <label for="sex" class=" text-uppercase  fw-bolder">Sex:</label>

                        </div>
                        <div class="col-3 mt-1">
                            <input type="radio" name="gender" id="sex">
                            <label for="">Male</label>
                            <input type="radio" name="gender" id="sex">
                            <label for="">Female</label>
                            <input type="radio" name="gender" id="sex">
                            <label for="">Others</label>

                        </div>

                        <div class="col-1 text-end">
                            <label for="age" class=" p-1 text-uppercase  fw-bolder">Age:</label>
                        </div>

                        <div class="col-2">
                            <input type="number" name="age" id="age" class="form-control" placeholder="Age">
                        </div>

                        <div class="col-2 text-end ">
                            <label for="date of birth" class="p-1 text-uppercase  fw-bolder">Date Of Birth:</label>
                        </div>

                        <div class="col-2">
                            <input type="date" name="date_of_birth" class="form-control">
                        </div>

                    </div>


                    <div class="row mt-4">

                        <div class="col-2 text-end">
                            <label for="disease" class="p-1 text-uppercase  fw-bolder">desiase:</label>
                        </div>

                        <div class="col-4">
                            <input type="text" name="disease" id="disease" class="form-control" placeholder="disease">
                        </div>

                        <div class="col-2 text-end">
                            <label for="room" class="p-1 text-uppercase  fw-bolder">Room No:</label>
                        </div>

                        <div class="col-4">
                            <select class="form-select" aria-label="multiple select example" name="room[]">
                                <option selected>401-BLock-D</option>
                                <option value="1">303-Block-f</option>
                                <option value="2">503-Block-e</option>
                            </select>
                        </div>
                    </div>


                    <div class="row mt-4">

                        <div class="col-2 text-end">
                            <label for="medicine" class="p-1 text-uppercase  fw-bolder">Medicine:</label>
                        </div>

                        <div class="col-4">
                            <select class="form-select" aria-label="multiple select example" name="medicine">
                                <option selected>Napa-10mg</option>
                                <option value="1">Finix-20mg</option>
                                <option value="2">Others</option>
                            </select>
                        </div>

                        <div class="col-3 text-end">
                            <label for="next_appointment"
                                class=" mt-1 text-uppercase  fw-bolder">next-appointment:</label>
                        </div>

                        <div class=" col-3">
                            <input type="date" name="next_appointment" id="next_appointmen" class="form-control">
                        </div>

                    </div>

                    <div class="row mt-4">

                        <div class="col-2"></div>
                        <div class="col-1 text-end mt-1">
                            <label for="test" class=" text-uppercase  fw-bolder">test:</label>
                        </div>
                        <div class="col-4">
                            <select class="form-select" aria-label="multiple select example" name="test">
                                <option selected>Napa-10mg</option>
                                <option value="1">Finix-20mg</option>
                                <option value="2">Others</option>
                            </select>
                        </div>
                        <div class="col-5"></div>
                    </div>


                    <div class="row mt-5">
                        <div class="col-3 text-end">
                            <label class="mt-3 text-uppercase  fw-bolder" for="message">Additional information:</label>
                        </div>


                        <div class="col-9">
                            <textarea class="form-control" name="message" id="message" rows="4"></textarea>
                        </div>

                    </div>

                    <div class="row mt-5">
                        <div class="col-5"></div>
                        <div class=" col-2  text-center">
                            <button type="submit"
                                class="text-uppercase  fw-bolder btn btn-outline-primary text-center p-2">Submit</button>
                        </div>
                        <div class="col-5"></div>

                    </div>


                </div>
            </form>
        </div>


        <!-- content End -->


        <!-- Footer Start -->
        <x-backend.partials.footer />
        <!-- Footer End -->

    </div>
    <!-- Content End -->

    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>



    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>

</x-backend.layout.master>
