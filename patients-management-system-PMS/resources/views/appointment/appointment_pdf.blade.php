<table class="table table table-striped  table table-bordered border-warning">
    <thead class="table-dark">
        <tr class="text-center">
            <th scope="col">Ser No</th>
            <th scope="col"> Patient_Name</th>
            <th scope="col">Doctor_Name</th>
            <th scope="col">Doctor_Category</th>
            <th scope="col">Appointment Date</th>
           
        
    </thead>
    <tbody>

       

        @foreach ($appointment as $key => $appointment)
            <tr class="text-center">
                <td scope="row">{{ $key + 1 }}</td>
                <td>{{ $appointment->patient_name }}</td>
                <td>{{ $appointment->doctor_name }}</td>
                <td>{{ $appointment->doctor_category }}</td>
                <td>{{ $appointment->appointment_date }}</td>
               

                
            </tr>
        @endforeach
    </tbody>
</table>


<style>

    table, th , td {
        padding: 20px;
        border: 1px solid black;
        border-collapse: collapse;   
    }

    table{
        width:100%;
    }


</style>