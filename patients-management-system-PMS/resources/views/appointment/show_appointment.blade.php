<x-backend.layout.master>

    <!-- Sidebar Start -->
    <x-backend.partials.sidebar />
    <!-- Sidebar End -->



    <!-- Content Start -->
    <div class="content">
        <!-- Navbar Start -->
        <x-backend.partials.navbar />
        <!-- Navbar End -->

        <!-- content Start -->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>

            </div>
        @endif

        @if (session('message'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>{{ session('message') }}!</strong>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('success') }}!</strong> <button type="button" class="btn-close"
                    data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif


        <div class="container mt-5">

            <div class="mb-3"><a href="{{ route('appointment.list') }}" class="btn btn-sm btn-primary">back</a></div>
            <div class="card card-body">

                <p>Patient Name: {{ $appointment->patient_name }}</p>
                <p>Doctor Name: {{ $appointment->doctor_name }}</p>
                <p>Doctor Category: {{ $appointment->doctor_category }}</p>
                <p>Phone: {{ $appointment->phone }}</p>
                <p>Gender: {{ $appointment->gender }}</p>
                <p>Age: {{ $appointment->age }}</p>
                <p>Description: {{ $appointment->description }}</p>
                <p>Appointment_Date: {{ $appointment->appointment_date }}</p>
                <p>Address: {{ $appointment->address }}</p>
                
                

            </div>



        </div>



        <!-- content End -->


        <!-- Footer Start -->
        <x-backend.partials.footer />
        <!-- Footer End -->

    </div>
    <!-- Content End -->

    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>




    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>


</x-backend.layout.master>