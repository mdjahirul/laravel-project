<?php include('./db.php') ?>
<?php
$id = $_GET['id'];

$sql = "SELECT * FROM students where id='$id'";
$stetments = $pdo->prepare($sql);
$stetments->execute();
$data = $stetments->fetch();



?>

<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap demo</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    </head>

    <body>

        <div class="container">
            <div class="col-3">
                <div class="card">
                    <div class="card-header bg-primary text-white"> Save Now</div>
                    <div class="card-body">
                        <form action="./update.php" method="POST">
                            <div class="form-group">
                                <label for="name"> Name</label>
                                <input class="form-control" value="<?= $data['name'] ?>" name="name" type="text">
                            </div>
                            <div class="form-group">
                                <label for="email"> Email</label>
                                <input name="email" class="form-control" value="<?= $data['email'] ?>" type="email">
                            </div>
                            <div class="form-group">
                                <label for="password"> Password</label>
                                <input name="password" class="form-control" value="<?= $data['password'] ?>" type="password">
                            </div>
                            <div>
                                <input type="hidden" name="id" value="<?= $data['id'] ?>">
                            </div>
                            <br>
                            <div>
                                <a href="./index.php" type="submit" class="btn btn-secondary"> Back</a>
                                <button type="submit" class="btn btn-success float-end"> Change Save </button>

                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>


        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
    </body>

</html>