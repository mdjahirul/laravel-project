@props(['name','label','option','select'])
<label for="{{ $name }}" class="form-label">{{ ucwords($label ?? '') }}</label><br>
<select name="{{ $name }}"  class="form-select" style="height: 38px; border:1px solid 	#DCDCDC; border-radius:5px; width:100%;">
    <option value="">Choose... </option>
    @foreach ($option as $key=>$value)
        <option value="{{ $key }}"{{ old($name,$select ?? '')== $key ?'selected':'' }}>{{ $value }}</option>
    @endforeach
    @error($name)
    <span class="sm text-danger">{{ $message }}</span>
    @enderror
</select>





{{-- <label for="name">Category</label><br>
<select name="category_id" class="form-select">
    <option value=""  style="border: 1px solid #DCDCDC;">Select One</option>
    @foreach ($categories as $category)
        <option value="{{ $category->id }}"{{ old('category_id')== $category->id ?'selected':'' }}>{{ $category->name }}</option>
    @endforeach
</select> --}}

{{-- <label for="{{ $name }}" class="form-label">{{ ucwords($name) }}</label><br>
<select name="{{ $name }}"  class="form-select" style="height: 38px; border:1px solid 	#DCDCDC; border-radius:5px; width:100%;">
    <option value="">Choose... </option>
    <option value="Dhaka" {{ old($name,$attributes) == 'Dhaka' ? 'selected': '' }}>Dhaka</option>
    <option value="Barisal" {{ old($name,$attributes) == 'Barisal' ? 'selected': '' }}>Barisal</option>
    <option value="Sylhet" {{ old($name,$attributes) == 'Sylhet' ? 'selected': '' }}>Sylhet</option>
    @error($name)
    <span class="sm text-danger">{{ $message }}</span>
@enderror
</select> --}}