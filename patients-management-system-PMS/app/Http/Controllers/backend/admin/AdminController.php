<?php

namespace App\Http\Controllers\backend\admin;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Exception;
use Illuminate\Contracts\Session\Session;

class AdminController extends Controller
{

    // Admin Login form function
    public function adminLoginForm()
    {
        return view('backend.admin.all_admin.admin_login');
    }
    // Admin Login Check function
    public function adminLogin(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
        // dd($request->all());

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect()->route('admin.deshboard');
        } else {
            return redirect()->back()->with('message', 'Access denied');
        }
    }
}
