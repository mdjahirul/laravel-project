<x-backend.layout.master>


    <div class="container-xxl position-relative bg-white d-flex p-0">
        {{-- <!-- Spinner Start -->
        <div id="spinner"
            class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <!-- Spinner End --> --}}


        <!-- Sign Up Start -->
        <div class="container-fluid">
            <div class="row h-100 align-items-center justify-content-center" style="min-height: 100vh;">
                <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4">
                    <div class="bg-light rounded p-4 p-sm-5 my-4 mx-3">

                        
                        <div class="d-flex align-items-center justify-content-between mb-3">

                            <h4 style="margin:auto;
                            text-align:center;
                            background-color: turquoise;
                            color: white;
                            font-family: none;
                            width: 100%;
                            font-weight: 600;
                            font-size: 28px;
                            height: 45px;
                            line-height: 45px;
                            border-radius: 50px;">User Register</h4>
                        </div>

                        <!-- Validation Errors -->
                        <x-auth-validation-errors class="mb-4" :errors="$errors" />

                        <form method="POST" action="{{ route('register') }}">
                            @csrf

                            <!-- Name -->
                            
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" id="name" required placeholder="Enter your name" name="name" value="{{ old('name') }}">
                                    <label for="name">Username</label>
                                </div>
                            <!-- Email Address -->


                            <div class="form-floating mb-3">
                                <input type="email" class="form-control" id="email" placeholder="Enter your email" name="email"
                                value="{{ old('email') }}" required>
                                <label for="email">Email address</label>
                            </div>

                            <div class="form-floating mb-3">
                                <input type="number" class="form-control" id="phone" placeholder="Enter your Phone Number" name="phone"
                                value="{{ old('phone') }}" required>
                                <label for="phone">Phone</label>
                            </div>

                           <!-- Password -->
                            <div class="form-floating mb-4">
                                <input type="password" class="form-control" id="password" placeholder=" Enter your password" name="password"
                                required>
                                <label for="password">Password</label>
                            </div>
                           

                            <!-- Confirm Password -->
                            <div class="form-floating mb-4">
                                <input type="password" class="form-control" id="password_confirmation" placeholder=" Enter your Confirm-password" name="password_confirmation"
                                required>
                                <label for="password_confirmation">Confirm Password</label>
                            </div>

                            <div class="flex items-center justify-end ">
                                <a class="underline text-sm text-gray-600 hover:text-gray-900"
                                    href="{{ route('login') }}">
                                    {{ __('Already registered?') }}
                                </a>

                                <button type="submit" class="btn btn-primary w-100 mb-2 mt-4">Register</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Sign In End -->
    </div>
</x-backend.layout.master>
