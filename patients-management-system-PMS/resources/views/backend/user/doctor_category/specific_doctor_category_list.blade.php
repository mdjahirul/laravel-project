<x-backend.layout.master>

    <!-- Sidebar Start -->
    <x-backend.partials.sidebar />
    <!-- Sidebar End -->



    <!-- Content Start -->
    <div class="content">
        <!-- Navbar Start -->
        <x-backend.partials.navbar />
        <!-- Navbar End -->

        <!-- content Start -->

        <div class="container">
            <div class="row mx-4">
                <h3 class="text-center mt-3">Doctor Specialist</h3>
                @foreach ($specific_doctor_category_list as $specific_doctor_category_list)
                    <div class="col col-3">
                        <div class="card" style="width: 15rem; margin:auto;">
                            <img src="..." class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">
                                    {{ ucfirst($specific_doctor_category_list->first_name)}} 
                                    {{ ucfirst($specific_doctor_category_list->last_name)}} 
                                </h5>
                                <b class="card-title">{{ 
                                ucfirst($specific_doctor_category_list->doctorCategory->name)
                                 }} Specialist</b>

                                <p class="card-text">Some quick example text to build on the card title and make up
                                    the
                                    bulk of the card's content.</p>
                                <a href="{{ route('user.doctor.specific',$specific_doctor_category_list->id) }}" class="btn btn-sm btn-primary">Show</a>
                                <a href="" class="btn btn-sm btn-success">Appointment</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <!-- content End -->


        <!-- Footer Start -->
        <x-backend.partials.footer />
        <!-- Footer End -->

    </div>
    <!-- Content End -->

    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>



    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>

</x-backend.layout.master>
