<x-backend.layout.master>

    <!-- Sidebar Start -->
    <x-backend.partials.sidebar />
    <!-- Sidebar End -->



    <!-- Content Start -->
    <div class="content">
        <!-- Navbar Start -->
        <x-backend.partials.navbar />
        <!-- Navbar End -->

        <!-- content Start -->

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('success') }}!</strong> <button type="button" class="btn-close"
                    data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
       
            <div  class="container  ">
             <form action="{{ route('admin.profile.update', auth('admin')->user()->id) }} " class="container" method="POST" enctype="multipart/form-data" >

                 @csrf

                <div class="container rounded  mt-5 mb-5  border border-1 border-success" style="background-color: rgba(83, 226, 27, 0.678)">
                    <div class="row">
                        <div class="col-md-3 border-right">
                            <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                                @if(isset(auth('admin')->user()->image))
                                    <img class="rounded-circle mt-5" style="width:150px; height:150px;" src="{{ asset('images/'.Auth('admin')->user()->image) }}">
                                @endif
                                

                                    <span class="font-weight-bold">{{ auth('admin')->user()->first_name  ?? (auth()->user()->name ?? '') }}</span><span
                                    class="text-black-50">{{ auth('admin')->user()->email  ?? (auth()->user()->name ?? '') }}</span><span> </span></div>
                        </div>
                        <div class="col-md-5 border-right">
                            <div class="p-3 py-5">
                                <div class="d-flex justify-content-between align-items-center mb-3">
                                    <h4 class="text-right text-uppercase">Profile-Details</h4>
                                </div>
                                <div class="row mt-2">

                                    <div class="col-md-6 ">
                                        <label  for="fast name" >First Name</label>
                                        <input type="text"id="fast name" name="fast_name" class="form-control" value="{{ auth('admin')->user()->first_name }}">
                                    </div>

                                    <div class="col-md-6">
                                        <label for="last name">Last Name</label>
                                        <input type="text" id="last name" name="last_name" class="form-control" value="{{ auth('admin')->user()->last_name }}" >
                                    </div>

                                </div>

                                <div class="row mt-3">

                                    <div class="col-md-12">
                                        <label class="labels">Mobile Number</label>
                                        <input type="text"  name="number" class="form-control" value="{{ auth('admin')->user()->phone }}">
                                    </div>
                                    <div class="col-md-12">
                                        <label class="labels mt-2">Doctor Category</label>
                                        <input type="text" name="doctor_category" class="form-control" value="{{ auth('admin')->user()->doctor_category }}">
                                    </div>
                                    
                                    <div class="col-md-12 mt-2"><label class="labels">Email</label><input
                                            type="email" class="form-control" name="email" value="{{ auth('admin')->user()->email }}"></div>
                                    <div class="col-md-12"><label class="labels mt-2">Gendr</label><input
                                            type="text" class="form-control" name="gender" value="{{ auth('admin')->user()->gender }}"></div>

                                    <div class="col-md-12">
                                        <label class="labels mt-2" for="image">Image</label>
                                        <input type="file" id="image"  class="form-control" name="image">
                                        </div>

                                    {{-- <div class="col-md-12"><label class="labels mt-2">Hospital
                                            Name</label><input type="text" class="form-control" value=""></div> --}}

                                </div>

                                <div class="mt-5 text-center">
                                    <button class="btn btn-outline-primary profile-button" type="submit">Update</button>
                                    <a href="{{ route('admin.deshboard') }}" class="btn btn-outline-danger profile-button">Close</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">

                        </div>
                    </div>
                </div>
             </form>
            </div> 


    <!-- content End -->


    <!-- Footer Start -->
      <x-backend.partials.footer />
    <!-- Footer End -->

    </div>
    <!-- Content End -->

    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>




    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>


</x-backend.layout.master>
