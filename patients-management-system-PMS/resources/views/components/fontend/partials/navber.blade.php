<nav class="navbar navbar-expand-lg navigation bg-info" id="navbar">
    <div class="container">
          <a class="navbar-brand" href="#">
              <img src="" alt="" class="img-fluid">
          </a>

          <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarmain" aria-controls="navbarmain" aria-expanded="false" aria-label="Toggle navigation">
        <span class="icofont-navigation-menu"></span>
      </button>
  
      <div class="collapse navbar-collapse" id="navbarmain">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link text-white" href="#">Home</a>
          </li>
           <li class="nav-item"><a class="nav-link text-white" href="#">About</a></li>
            <li class="nav-item"><a class="nav-link text-white" href="#">Services</a></li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-white" href="#" id="dropdown02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Department</a>
                <
              </li>

              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-white" href="#" id="dropdown03" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Doctors</a>
               
              </li>

           <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-white" href="#" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Blog </a>
                
              </li>
           <li class="nav-item">
               <a class="nav-link text-white" href="{{ route('login') }}">Login</a>
               
            </li>
        </ul>
      </div>
    </div>
</nav>