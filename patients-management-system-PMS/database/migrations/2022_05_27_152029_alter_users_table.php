<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('phone');
            $table->unsignedBigInteger('doctor_id')->nullable();
            $table->unsignedBigInteger('doctor_category_id')->nullable();
            $table->unsignedBigInteger('role_id')->default(4);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('phone');
            $table->dropColumn('doctor_id');
            $table->dropColumn('doctor_category_id');
            $table->dropColumn('role_id');
        });
    }
}
