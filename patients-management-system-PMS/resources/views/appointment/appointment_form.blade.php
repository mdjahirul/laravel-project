<x-backend.layout.master>

    <!-- Sidebar Start -->
    <x-backend.partials.sidebar />
    <!-- Sidebar End -->



    <!-- Content Start -->
    <div class="content">
        <!-- Navbar Start -->
        <x-backend.partials.navbar />
        <!-- Navbar End -->

        <!-- content Start -->

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('success') }}!</strong> <button type="button" class="btn-close"
                    data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif

        <div class="container mt-4">

            <section class=" appoinment p-4 rounded-1" style="background-color: #71b4b4">
                <div class="container">
                    <div class="row ">

                        <div class="col-lg-3 ">

                        </div>

                        <div class="col-lg-6 col-md-10 ">
                            <div class=" mt-5 mt-lg-0">
                                <h2 class="mb-2 text-center ">Book appoinment</h2>
                                <p class="mb-4 text-white">Mollitia dicta commodi est recusandae iste, natus eum
                                    asperiores corrupti qui velit . Iste dolorum atque similique praesentium soluta.
                                </p>

                                <form class="" method="post" action="{{ route('appointment.store') }}">
                                    @csrf

                                    <input type="hidden" name="doctor_id" id="" value="{{ $doctor->id }}">

                                    <input type="hidden" name="doctor_name" value="{{ $doctor->first_name }} {{ $doctor->last_name }}" >

                                    <input type="hidden" name="doctor_category" value="{{ $doctor->doctor_category }}">

                                    <div class="row">
                                       

                                        
                                        <div class="col-lg-6 mt-3 ">
                                            <div class="text-start">
                                                <h6 class="title-color text-underline"><u>Patient Name:</u> </h6>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="patient_name" id="patient_name"
                                                    class="form-control" placeholder="Patient Name">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 mt-3 ">
                                            <div class="text-start">
                                                <h6 class="title-color text-underline"><u>Phone Number:</u> </h6>
                                            </div>
                                            <div class="form-group">
                                                <input type="number" name="phone" id="phone" class="form-control"
                                                    placeholder="Phone Number">
                                            </div>
                                        </div>

                                        <div class="col-lg-6 mt-3">
                                            <div class="text-start">
                                                <h6 class="title-color text-underline"><u>Appointment Date:</u> </h6>
                                            </div>
                                            <div class="form-group">
                                                <input name="appointment_date" id="date" type="date"
                                                    class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-lg-6 mt-3">
                                            <div class="form-group">
                                                <div class="text-start">
                                                    <h6 class="title-color text-underline">Age:</h6>
                                                </div>
                                                <input name="age" id="time" type="number" class="form-control"
                                                    placeholder="Age">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 mt-3">
                                            <div class="text-start">
                                                <h6 class="title-color text-underline"><u>Gender:</u> </h6>
                                            </div>
                                            <div class="text-start">
                                                <input name="gender" id="name" type="radio" class=""
                                                    placeholder="Full Name">
                                                <label for="">
                                                    <h6>Male</h6>
                                                </label>
                                                <input name="gender" id="name" type="radio" class=""
                                                    placeholder="Full Name">
                                                <label for="">
                                                    <h6>Female</h6>
                                                </label>
                                                <input name="gender" id="name" type="radio" class=""
                                                    placeholder="Full Name">
                                                <label for="">
                                                    <h6>Others</h6>
                                                </label>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group mb-4">
                                        <div class="text-start">
                                            <h6 class="title-color text-underline"><u>Patient Address:</u> </h6>
                                        </div>
                                        <textarea name="address" id="address" class="form-control" rows="3"
                                            placeholder="House No/Rood No/Area/City-City-code"></textarea>
                                    </div>

                                    <div class="form-group-2 mb-4">
                                        <div class="text-start">
                                            <h6 class="title-color text-underline"><u>About Problem:</u> </h6>
                                        </div>
                                        <textarea name="description" id="description" class="form-control" rows="6" placeholder="Your Message"></textarea>
                                    </div>

                                    <button type="submit" class="btn btn-main btn-round-full btn-primary" >Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>


        <!-- content End -->


        <!-- Footer Start -->
        <x-backend.partials.footer />
        <!-- Footer End -->

    </div>
    <!-- Content End -->

    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>




    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>


</x-backend.layout.master>
