<?php

namespace App\Http\Controllers\backend\admin;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;

class AdminProfileController extends Controller
{
    public function adminProfileUpdate(Request $request, $id)
    {

        $request->validate([
            'fast_name' => 'required|min:2|max:255',
            'last_name' => 'required',
            'number' => 'required',
            'email' => 'required',
            'gender' => 'required',
        ]);

        // dd($request->all());
        try {
            $admin  = Admin::find($id);

            $requestData = [
                'first_name' => $request->fast_name,
                'last_name' => $request->last_name,
                'number' => $request->number,
                'email' => $request->email,
                'gender' => $request->gender,
            ];

            if ($request->hasFile('image')) {
                // unlink("images/".$admin->image);
                $requestData['image'] = $this->uploadImage(request()->file('image'));
            }

            $admin->update($requestData);

            return redirect()->back()->with('success', "Profile Update Successfully");
        } catch (QueryException $e) {
            dd($e->getMessage());
        }
    }

    public function uploadImage($file)
    {
        $fileName = time() . '.' . $file->getClientOriginalExtension();
        $file->move(public_path('images'), $fileName);
        return $fileName;
    }

    public function adminProfile()
    {

        return view('backend.admin.all_admin.profile');
    }
}
