<x-backend.layouts.master>


    <x-slot name="page_title">
        Products
    </x-slot>
    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Products
            </x-slot>
            <x-slot name="add">  
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ Route('products.index') }}">Products</a></li>
            <li class="breadcrumb-item active">Edit</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>

    <div class="card mb-4">
        <div class="card-header ">
            <div class="d-flex justify-content-between">
                <span><i class="fas fa-table me-1"></i>Products</span>
                <span>
                    <a class="btn btn-primary text-left" href="{{ Route('products.index') }}" role="button">List</a>
                </span>
            </div>
        </div>
        <div class="card-body">
            <x-backend.layouts.elements.errors :errors="$errors"/>
            <form action="{{ route('products.update', ['product' => $product->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('patch')
                <div class="row mb-3">
                    <div>
                        <x-backend.form.input name="name" :value="old('name',$product->name)"/>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.input name="category_name" :value="old('category_name',$product->category_name)"/>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.input name="brand_name" :value="old('brand_name',$product->brand_name)"/>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.textarea name="description">
                            {{ old('description',$product->description) }}
                        </x-backend.form.textarea>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.input name="image" type="file"/>
                    </div>
                
                </div>
                <div class="mt-4 mb-0">
                    <div class="d-grid"><button onclick="return confirm('Are you sure want to update ?')" class="btn btn-primary btn-block" type="submit">Update</button></div>
                </div>
            </form>
        </div>
    </div>

    @push('script')
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <script>
        tinymce.init({
          selector: '#long_description'
        });
      </script>
      @endpush

</x-backend.layouts.master>