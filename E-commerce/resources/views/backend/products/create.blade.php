<x-backend.layouts.master>
    <x-slot name="page_title">
        Products
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Products
            </x-slot>
            <x-slot name="add">
                
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('products.index') }}">Products</a></li>
            <li class="breadcrumb-item active">Create</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>

    <div class="card mb-4">
        <div class="card-header ">
            
            <div class="d-flex justify-content-between">
                <span><i class="fas fa-table me-1"></i>Products</span>
                <span>
                    <a class="btn btn-primary text-left" href="{{ Route('products.index') }}" role="button">List</a>
                </span>
            </div>
        </div>
        <div class="card-body">
            <form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row mb-3">
                    <div class="col-md-6 mb-2">
                        <x-backend.form.input name="name" :value="old('name')"/>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.input name="category_name" :value="old('category_name')"/>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.input name="brand_name" :value="old('brand_name')"/>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.textarea name="description">
                            {{ old('description') }}
                        </x-backend.form.textarea>
                    </div>

                   
                    <div class="col-md-6 mb-2">
                        <x-backend.form.input name="image" type="file"/>
                    </div>

                </div>
                <div class="mt-4 mb-0">
                    <div class="d-grid"><button class="btn btn-primary btn-block" type="submit">Save</button></div>
                </div>
            </form>
        </div>
    </div>

</x-backend.layouts.master>