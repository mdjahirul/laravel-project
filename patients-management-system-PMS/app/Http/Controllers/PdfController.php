<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use Mpdf\Mpdf;
use App\Models\Prescription;
use Illuminate\Http\Request;

class PdfController extends Controller
{
    public function prescription_Pdf(){

        $prescription = Prescription::all(); 
 
        $fileName = 'prescription.pdf';
        $html = view('backend.admin.prescription.prescription_Pdf', compact('prescription'))->render();
        $mpdf = new \Mpdf\Mpdf();
     //    $mpdf->addPage("L");
        $mpdf->WriteHTML($html);
        $mpdf->Output($fileName,'i');
 
     }

    public function appointment_Pdf(){

        $appointment = Appointment::all(); 
 
        $fileName = 'prescription.pdf';
        $html = view('appointment.appointment_Pdf', compact('appointment'))->render();
        $mpdf = new \Mpdf\Mpdf();
     //    $mpdf->addPage("L");
        $mpdf->WriteHTML($html);
        $mpdf->Output($fileName,'i');
 
     }
}
