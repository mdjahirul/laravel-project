<?php include('./db.php') ?>
<?php
$sql = "SELECT * FROM students";
$stetments = $pdo->prepare($sql);
$stetments->execute();
$data = $stetments->fetchAll(PDO::FETCH_ASSOC);

?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
</head>

<body>



    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <h2 class="text-center">PHONEBOOK</h2>

            </div>
            <div class="col-3">
                <div class="card">
                    <div class="card-header bg-primary text-white"> Save Now</div>
                    <div class="card-body">
                        <form action="./insert.php" method="POST">
                            <div class="form-group">
                                <label for="name"> Name</label>
                                <input class="form-control" name="name" type="text">
                            </div>
                            <div class="form-group">
                                <label for="email"> Email</label>
                                <input name="email" class="form-control" type="email">
                            </div>
                            <div class="form-group">
                                <label for="password"> Password</label>
                                <input name="password" class="form-control" type="password">
                            </div>
                            <br>
                            <button type="submit" class="btn btn-success"> submit</button>
                        </form>
                    </div>
                </div>

            </div>

            <div class="col-9">
                <div class="card">
                    <div class="card-header bg-primary text-white">Output data</div>
                    <table class="table table-bordered ">


                        <thead>
                            <tr>
                                <th scope="col">SL</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Password</th>
                                <th scope="col" class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody class="table-group-divider">
                            <?php $i = 1;
                            foreach ($data as $value) { ?>
                                <tr>
                                    <!-- <th scope='row'></th> -->

                                    <td><?= $i++ ?></td>
                                    <td><?= $value['name'] ?></td>
                                    <td><?= $value['email'] ?></td>
                                    <td><?= $value['password'] ?></td>
                                    <td class='text-center'>
                                        <a href="./edit.php?id=<?= $value['id'] ?>" class='btn btn-warning' type='submit'>Edit </a>

                                        <a href="./delete.php?id=<?=$value['id']?>" class='btn btn-danger' type='submit'> Delete</a>



                                    </td>
                                </tr>
                            <?php }; ?>

                        </tbody>
                    </table>





                </div>

            </div>

        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
</body>

</html>