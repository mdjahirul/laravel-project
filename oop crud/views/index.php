<?php include('../vendor/autoload.php');
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

use jahirul\Student;

$student = new Student;

$info = $student->index();
if ($_SERVER['REQUEST_METHOD'] == "GET" && isset($_GET['id'])) {
    $editData = $student->edit($_GET['id']);
}elseif ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['id']) && $_POST['method'] == 'delete') {
    $student->delete($_POST);
}elseif ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['method']) && $_POST['method'] == 'PUT') {
    $student->update($_POST);
} 
 elseif ($_SERVER['REQUEST_METHOD'] == "POST") {
    $student->store($_POST);
} 

?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OOP CRUD</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/010be078f0.js" crossorigin="anonymous"></script>
</head>

<body>



    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <h2 class="text-center">STUDENT INFORMATION</h2>


            </div>


            <div class="col-3">

                <div class="card">
                    <div class="card-header bg-primary text-white"> Save Now</div>
                    <div class="card-body">
                        <form action="" method="POST">
                            <?php if(isset($editData)) : ?>
                                <input type="hidden" name="method" value="PUT">
                                <input type="hidden" name="id" value="<?= $editData['id'] ?>">
                            <?php endif; ?>
                            <div class="form-group">
                                <label for="fast_name">Fast Name</label>
                                <input class="form-control" name="fast_name" type="text" value="<?= isset($editData) ? $editData['fast_name'] : '' ?>">
                            </div>
                            <div class="form-group">
                                <label for="last_name"> Last Name</label>
                                <input name="last_name" class="form-control" type="text" value="<?= isset($editData) ? $editData['last_name'] : '' ?>">
                            </div>
                            <div class="form-group">
                                <label for="phone_number"> Phone Number</label>
                                <input name="phone_number" class="form-control" type="number" value="<?= isset($editData) ? $editData['phone'] : '' ?>">
                            </div>
                            <div class="form-group">
                                <label for="roll_number"> Roll Number</label>
                                <input name="roll_number" class="form-control" type="number" value="<?= isset($editData) ? $editData['roll_number'] : '' ?>">
                            </div>
                            <br>
                            
                            <?php
                            if (isset($editData)) {
                                echo  "<button type='submit' class='btn btn-primary'>Save Change </button>";
                                echo  "<a href='./index.php' type='submit' class='btn btn-secondary mx-2'>Reset </a>";

                                
                            }else {
                               echo "<button type='submit' class='btn btn-success'> submit</button>";
                            }
                            ?>

                        </form>
                    </div>
                </div>

            </div>


            <div class="col-9">
                <?php
                if (isset($_SESSION['msg'])) {
                    $msg = $_SESSION['msg'];
                    echo "<P id='alert-msg' style='color:green'> $msg</p>";
                    session_destroy();
                }

                ?>

                <div class="card">


                    <div class="card-header bg-primary text-white">Output data</div>
                    <table class="table table-bordered ">


                        <thead>
                            <tr>

                                <th scope="col">SL </th>
                                <th scope="col">Fast Name </th>
                                <th scope="col">Last Name</th>
                                <th scope="col">Phone Number</th>
                                <th scope="col">Roll No</th>
                                <th scope="col" class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody class="table-group-divider">
                            <?php
                            $i = 1;
                            foreach ($info as $value) { ?>
                                <tr>
                                    <!-- <th scope='row'></th> -->

                                    <td><?= $i++ ?></td>
                                    <td><?= $value['fast_name'] ?? '' ?></td>
                                    <td><?= $value['last_name'] ?? ''  ?></td>
                                    <td><?= $value['phone'] ?? '' ?></td>
                                    <td><?= $value['roll_number'] ?? '' ?></td>
                                    <td class='text-center'>

                                        <a href="./index.php?id=<?= $value['id'] ?>" class='btn btn-warning' type='submit'><i class="fa-solid fa-pen-nib"></i> </a>

                                        <form action="" method="POST" style="display:inline-block ;">
                                            <input type="hidden" name="id" value="<?= $value['id'] ?>">
                                            <input type="hidden" name="method" value="delete">
                                            <button class='btn btn-danger' type='submit'><i class="fa-solid fa-trash-can"></i></button>
                                        </form>

                                    </td>
                                </tr>
                            <?php }; ?>
                        </tbody>
                    </table>





                </div>

            </div>

        </div>
    </div>

    <script>
        setTimeout(function() {
            document.getElementById('alert-msg').style.display = 'none';
        }, 1500);
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
</body>