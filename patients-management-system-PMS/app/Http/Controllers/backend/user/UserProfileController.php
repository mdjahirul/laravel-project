<?php

namespace App\Http\Controllers\backend\user;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserProfileController extends Controller
{



    public function UserProfile(){
     
        return view('backend.user.profile');
     }

     public function Userlist()
     {
        $patients = User::all();
        return view('backend.user.alluserlist',compact('patients'));
     }
}
