<!-- Sidebar Start -->
<div class="sidebar pe-4 pb-3">
    <nav class="navbar bg-light navbar-light">
        <a href="#" class="navbar-brand mx-4 mb-3">
            <h3 class="text-primary"><i class="fa me-2"></i>DASHMIN</h3>
        </a>
        <div class="d-flex align-items-center ms-4 mb-4">
            <div class="position-relative">

                @if (isset(auth('admin')->user()->image))
                    <img class="rounded-circle" src="{{ asset('images/' . Auth('admin')->user()->image) }}" alt=""
                        style="width: 40px; height: 40px;">
                @endif

                <div class="bg-success rounded-circle border border-2 border-white position-absolute end-0 bottom-0 p-1">
                </div>
            </div>
            <div class="ms-3">
                <h6 class="mb-0">{{ auth('admin')->user()->first_name ?? (auth()->user()->name ?? '') }}
                </h6>
                <span></span>
            </div>

        </div>
        <div class="navbar-nav w-100">
            @if (auth('admin')->user())
                <a href="{{ route('admin.deshboard') }}" class="nav-item nav-link active"><i
                        class="fa fa-tachometer-alt me-2"></i>Dashboard</a>
            @else
                <a href="{{ route('user.deshboard') }}" class="nav-item nav-link active"><i
                        class="fa fa-tachometer-alt me-2"></i>Dashboard</a>
            @endif







            @if (auth('admin')->user())
                <a href="" class="nav-item nav-link "><i class="fa fa-tachometer-alt me-2"></i>doctor Category</a>
                <a href="{{ route('appointment.list') }}" class="nav-item nav-link "><i class="fa fa-tachometer-alt me-2"></i>Appointment</a>

                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"><i
                            class="far fa-file-alt me-2"></i>Patient</a>
                    <div class="dropdown-menu bg-transparent border-0">
                        <a href="{{ route('patient.all') }}" class="dropdown-item">Patient</a>
                        <a href="{{ route('admin.prescription.list') }}" class="dropdown-item">prescription</a>

                    </div>
                </div>

                <a href="{{ route('user.all.list') }}" class="nav-item nav-link"><i
                        class="fa fa-keyboard me-2"></i>Users</a>
                <a href="{{ route('admin.profile') }}" class="nav-item nav-link"><i
                        class="fa fa-keyboard me-2"></i>Profile</a>
            
                <a href="{{ route('user.profile') }}" class="nav-item nav-link"><i
                        class="fa fa-keyboard me-2"></i>Profile</a>
            @endif



            @if (auth('admin')->user())
                @if (auth('admin')->user()->adminRole->id == 1)
                    <a href="{{ route('admin.all') }}" class="nav-item nav-link"><i class="far fa-file-alt me-2"></i>
                        Alladmin</a>
                @endif



                <a href="table.html" class="nav-item nav-link"><i class="far fa-file-alt me-2"></i>Medicine</a>
                <a href="table.html" class="nav-item nav-link"><i class="far fa-file-alt me-2"></i>Room</a>


                <a href="table.html" class="nav-item nav-link"><i class="far fa-file-alt me-2"></i>Test</a>
            @endif


            {{-- <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"><i
                        class="far fa-file-alt me-2"></i>Pages</a>
                <div class="dropdown-menu bg-transparent border-0">
                    <a href="signin.html" class="dropdown-item">Sign In</a>
                    <a href="signup.html" class="dropdown-item">Sign Up</a>
                    <a href="404.html" class="dropdown-item">404 Error</a>
                    <a href="blank.html" class="dropdown-item">Blank Page</a>
                </div>
            </div> --}}

        </div>
    </nav>
</div>
<!-- Sidebar End -->
