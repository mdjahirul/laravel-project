<?php

namespace App\Http\Controllers\backend\user;

use session;
use Illuminate\Http\Request;
use App\Models\DoctorCategory;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Support\Facades\Auth;

class UserDeshboardController extends Controller
{
    public function UserDeshboard(){
      
        $doctors = Admin::where('role_id','2')
        ->get();
        return view('backend.user.deshboard.user_deshboard',compact('doctors'));
    }
    
}
