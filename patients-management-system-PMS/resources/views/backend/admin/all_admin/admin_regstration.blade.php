@push('css')
    <style>
        .gradient-custom {
            /* fallback for old browsers */
            background: #f093fb;

            /* Chrome 10-25, Safari 5.1-6 */
            background: -webkit-linear-gradient(to bottom right, rgba(240, 147, 251, 1), rgba(245, 87, 108, 1));

            /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
            background: linear-gradient(to bottom right, rgba(240, 147, 251, 1), rgba(245, 87, 108, 1))
        }

        .card-registration .select-input.form-control[readonly]:not([disabled]) {
            font-size: 1rem;
            line-height: 2.15;
            padding-left: .75em;
            padding-right: .75em;
        }

        .card-registration .select-arrow {
            top: 13px;
        }

    </style>
@endpush

<!-- content Start -->

<x-backend.layout.master>
    <div class="container">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('message'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>{{ session('message') }}!</strong>
            </div>
        @endif

        <form action="{{ route('admin.register.store') }}" method="post">
            @csrf

            <div class="container">
                <div class="card-body p-4 p-md-5">
                    <h3 class="mb-4 pb-2 pb-md-0 mb-md-5">Registration Form</h3>
                    <form>
                        <div class="row">
                            <div class="col-md-6 mb-4">
                                <div class="form-outline">
                                    <input type="text" id="firstName" class="form-control form-control-md"
                                        name="first_name" value="{{ old('first_name') }}" />
                                    <label class="form-label" for="firstName">First Name</label>

                                </div>
                                @error('first_name')
                                    <div class="alert text-danger" style="margin: 0px;padding: 0px;">
                                        {{ $message }}</div>
                                @enderror

                            </div>
                            <div class="col-md-6 mb-4">
                                <div class="form-outline">
                                    <input type="text" id="lastName" class="form-control form-control-md"
                                        name="last_name" value="{{ old('last_name') }}" />
                                    <label class="form-label" for="lastName">Last Name</label>
                                </div>
                                @error('last_name')
                                    <div class="alert text-danger" style="margin: 0px;padding: 0px;">{{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 mb-4 pb-2">
                                <div class="form-outline">
                                    <input type="email" id="emailAddress" class="form-control form-control-md"
                                        name="email" value="{{ old('email') }}" />
                                    <label class="form-label" for="emailAddress">Email</label>
                                </div>
                                @error('email')
                                    <div class="alert text-danger" style="margin: 0px;padding: 0px;">{{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-4 pb-2">
                                <div class="form-outline">
                                    <input type="number" id="phone" class="form-control form-control-md" name="phone"
                                        value="{{ old('phone') }}" />
                                    <label class="form-label" for="phone">Phone Number</label>
                                </div>
                                @error('phone')
                                    <div class="alert text-danger" style="margin: 0px;padding: 0px;">{{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 mb-4 pb-2">
                                <div class="form-outline">
                                    <input type="password" id="password" class="form-control form-control-md"
                                        name="password" />
                                    <label class="form-label" for="password">Password</label>
                                </div>
                                @error('password')
                                    <div class="alert text-danger" style="margin: 0px;padding: 0px;">{{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-4 pb-2">
                                <div class="form-outline">
                                    <input type="password" id="confirm_password" class="form-control form-control-md"
                                        name="confirm_password" />
                                    <label class="form-label" for="confirm_password">Confirm
                                        Password</label>
                                </div>
                                @error('confirm_password')
                                    <div class="alert text-danger" style="margin: 0px;padding: 0px;">{{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 mb-4">


                                <select class="form-select" aria-label=" select example" name="doctor_category">
                                    <option selected> Choose option</option>
                                    <option value="Pathology">Pathology</option>
                                    <option value="kisu_akta">kisu_akta</option>
                                    <option value="janina">janina</option>
                                    <option value="janina">Hobe r ki</option>
                                    <option value="janina">Bujle Hobe</option>

                                </select>



                                <label class="form-label select-label">Doctor Category</label>
                            </div>
                            <div class="col-md-6 mb-4">
                                <h6 class="mb-2 pb-1">Gender: </h6>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="gender" id="maleGender"
                                        value="male" checked />
                                    <label class="form-check-label" for="maleGender">Male</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="gender" id="femaleGender"
                                        value="female" />
                                    <label class="form-check-label" for="femaleGender">Female</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="gender" id="otherGender"
                                        value="other" />
                                    <label class="form-check-label" for="otherGender">Other</label>
                                </div>
                                @error('gender')
                                    <div class="alert text-danger" style="margin: 0px;padding: 0px;">{{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 mb-4">
                                <select class="form-select" aria-label=" select example" name="role">
                                    <option selected> Choose option</option>
                                    @foreach ($roles as $role)
                                        <option value="{{ $role->id }}">
                                            {{ $role->name }}</option>
                                    @endforeach
                                </select>
                                <label class="form-label select-label">Role</label>
                            </div>

                            <div class="col-md-6 mb-4 pb-2">
                                <div class="form-outline">
                                    <textarea type="text" id="description" class="form-control form-control-md"
                                        name="description"> </textarea>
                                    <label class="form-label" for="confirm_password">Description</label>
                                </div>
                                @error('confirm_password')
                                    <div class="alert text-danger" style="margin: 0px;padding: 0px;">{{ $message }}
                                    </div>
                                @enderror
                            </div>


                        </div>
                        <div class="row">
                            <div class="">
                                <input class="btn btn-primary btn-md" type="submit" value="Submit" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </form>


        <!-- content End -->



</x-backend.layout.master>
