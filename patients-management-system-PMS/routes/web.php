<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PdfController;
use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\PrescriptionController;
use App\Http\Controllers\fontend\FontendController;
use App\Http\Controllers\backend\admin\AdminController;
use App\Http\Controllers\backend\admin\AllAdminController;
use App\Http\Controllers\backend\admin\AllPatientController;
use App\Http\Controllers\backend\user\UserProfileController;
use App\Http\Controllers\backend\admin\AdminProfileController;
use App\Http\Controllers\backend\user\UserDeshboardController;
use App\Http\Controllers\backend\user\DoctorCategoryController;
use App\Http\Controllers\backend\admin\AdminDeshboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('fontend.dashboard');
});

// Route::gat('/',[FontendController::class, 'dashboard']);

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__ . '/auth.php';



// Admin Controller
Route::prefix('admin')->name('admin.')->controller(AdminController::class)->group(function () {
    Route::get('/', 'adminLoginForm')->name('login.form');
    Route::post('/login', 'adminLogin')->name('login');
});

Route::middleware("admin")->group(function () {
    // Admin Deshboard Controller
    Route::prefix('admin')->name('admin.')->controller(AdminDeshboardController::class)->group(function () {
        Route::get('/register', 'adminRegstrationForm')->name('register.form');
        Route::post('/register/store', 'adminRegstrationStore')->name('register.store');
        Route::get('/deshboard', 'adminDeshboard')->name('deshboard');
        Route::get('/logout', 'adminLogout')->name('logout');
    });

    // Admin Profile Controller
    Route::prefix('admin')->name('admin.')->controller(AdminProfileController::class)->group(function () {
        Route::get('/profile', 'adminProfile')->name('profile');
        Route::post('/update/{id}', 'adminProfileUpdate')->name('profile.update');
    });


    //  All Admin Controller
    Route::prefix('admin')->name('admin.')->controller(AllAdminController::class)->group(function () {
        Route::get('/all/admins', 'allAdmins')->name('all');
        Route::get('/show/{id}', 'showAdmin')->name('show');
        Route::get('/edit/{id}', 'editAdmin')->name('edit');
        Route::put('/update/{id}', 'updateAdmin')->name('update');
        Route::delete('/delete/{id}', 'adminDelete')->name('destroy');
       
    });


    // All Patients Controller
    Route::prefix('patient')->name('patient.')->controller(AllPatientController::class)->group(function () {
        Route::get('/all', 'allPatients')->name('all');
        Route::get('/show/{id}', 'showPatient')->name('show');
        Route::get('/prescription/{id}', 'prescription')->name('prescription');
    });


    // User Profile Controller
    Route::prefix('user')->name('user.')->controller(UserProfileController::class)->group(function () {
        Route::get('/profile', 'UserProfile')->name('profile');
        Route::get('/profile.update', 'UserProfile')->name('profile.update');
        Route::get('/list', 'Userlist')->name('all.list');
    });
});


Route::middleware('auth')->group(function () {

    // User Deshboard Controller
    Route::prefix('user')->name('user.')->controller(UserDeshboardController::class)->group(function () {
        Route::get('/deshboard', 'UserDeshboard')->name('deshboard');
    });

    // Doctor Specialist Controller
    Route::prefix('doctor')->name('user.')->controller(DoctorCategoryController::class)->group(function () {
        Route::get('/list/{id}', 'doctorList')->name('specific.doctor.list');
        Route::get('/specific/show/{id}', 'doctorSpecific')->name('doctor.specific');
    });
});



//pascription controller

Route::prefix('admin')->name('admin.')->controller(PrescriptionController::class)->middleware('admin')->group(function () {
    Route::get('/prescription/list', 'prescription_list')->name('prescription.list');
    Route::get('/prescription/create/{id}', 'patients_prescription')->name('prescription.create');
    Route::post('/prescription/store', 'store')->name('prescription.store');
    Route::get('/prescription/show/{id}', 'show')->name('prescription.show');
});

// pdf 
    
Route::get('/prescription/Pdf', [PdfController::class, 'prescription_Pdf'])->name('prescription.pdf');
Route::get('/appointment/Pdf', [PdfController::class, 'appointment_Pdf'])->name('appointment.pdf');
    

//appointment controller

Route::prefix('appointment')->name('appointment.')->controller(AppointmentController::class)->group(function () {
    // Route::get('/', 'appointment_list')->name('list');
    Route::get('/', 'index')->name('list');
    Route::get('/create/{id}', 'appointment')->name('create');
    Route::get('/show/{id}', 'appointment_show')->name('show');
    Route::post('/store', 'store')->name('store');
    Route::get('/today', 'today_appointment')->name('today');
});
