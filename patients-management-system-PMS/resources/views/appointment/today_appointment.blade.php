{{-- @foreach ($appointment_today as $value)
{{ $value->patient_name }}
@endforeach --}}


<x-backend.layout.master>

    <!-- Sidebar Start -->
    <x-backend.partials.sidebar />
    <!-- Sidebar End -->



    <!-- Content Start -->
    <div class="content">
        <!-- Navbar Start -->
        <x-backend.partials.navbar />
        <!-- Navbar End -->

        <!-- content Start -->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('message'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>{{ session('message') }}!</strong>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('success') }}!</strong> <button type="button" class="btn-close"
                    data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif


        <div class="container mt-5">


            <table class="table table table-striped  table table-bordered border-warning">
                <thead class="table-dark">
                    <tr class="text-center">
                        <th scope="col">Ser No</th>
                        <th scope="col"> Patient_Name</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Gender</th>
                        <th scope="col">Age</th>
                        <th scope="col" class="text-center">Action</th>
                        {{-- <th scope="col">Age</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Test</th> --}}
                        {{-- <th scope="col">Disease</th> --}}
                        {{-- <th scope="col">Gender</th>
                        <th scope="col">Next_appointment</th>
                        <th scope="col">Message</th>
                         --}}
                    </tr>
                </thead>
                <tbody>

                   

                    @foreach ($appointment_today as $key => $value)
                        <tr class="text-center">
                            <td scope="row">{{ $key + 1 }}</td>
                            <td>{{ $value->patient_name }}</td>
                            <td>{{ $value->phone }}</td>
                            <td>{{ $value->gender }}</td>
                            <td>{{ $value->age }}</td>
                            <td class="text-center">

                                <a href="#" class="btn btn-sm btn-warning">SHOW</a>
                                <a class="btn btn-primary" href="{{ route('admin.prescription.create', $value->id)}}">Prescription</a>
                              
                               
                            </td>

                            {{-- <td>{{ $appointment->age }}</td>
                            <td>{{ $appointment->phone }}</td>
                            <td>{{ $appointment->test }}</td> --}}
                            {{-- <td>{{ $appointment->disease }}</td> --}}
                            {{-- <td>{{ $appointment->gender }}</td>
                            <td>{{ $appointment->next_appointment }}</td>
                            <td>{{ $appointment->message }}</td> --}}
                            
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <!-- content End -->


        <!-- Footer Start -->
        <x-backend.partials.footer />
        <!-- Footer End -->

    </div>
    <!-- Content End -->

    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>



    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>

</x-backend.layout.master>