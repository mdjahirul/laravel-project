<?php

namespace App\Http\Controllers\backend\admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AllPatientController extends Controller
{
    public function allPatients()
    {

        $patients = User::all();

        return view('backend.admin.patients.index', ['patients' => $patients]);
    }



    public function prescription()
    {

        $patients = User::all();
        return view('backend.admin.patients.index', ['patients' => $patients]);
    }
}
