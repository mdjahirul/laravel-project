<?php

namespace App\Http\Controllers\backend\admin;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DoctorCategory;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use SebastianBergmann\ObjectReflector\Exception;

class AdminDeshboardController extends Controller
{

    


    // Admin Regstration form function
    public function adminRegstrationForm()
    {
        $doctor_categories = DoctorCategory::all();
        $roles = Role::all();
        return view('backend.admin.all_admin.admin_regstration',compact('doctor_categories', 'roles'));
    }

    // Admin Regstration Store function
    public function adminRegstrationStore(Request $request)
    {

        $request->validate([
            'first_name' => 'required|max:20|min:2',
            'last_name' => 'required|max:50|min:3',
            'email' => 'required|email|unique:admins',
            'phone' => 'required|numeric',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
            'gender' => 'required',
            'doctor_category' => 'required',
            'role' => 'required',
            'description' => 'required',
        ]);
        try {
            // dd($request->all());

            $admin = Admin::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => Hash::make($request->password),
                'confirm_password' => Hash::make($request->confirm_password),
                'gender' => $request->gender,
                'doctor_category' => $request->doctor_category,
                'role_id' => $request->role,
                'description' => $request->description

            ]);
            return redirect()->route('admin.all')->with('success', "Admin Create Successfully");
        } catch (Exception $e) {
            return redirect()->back($e->getMessage());
        }
    }

    public function adminDeshboard()
    {
        $admin = Admin::all();
        // dd(auth('admin')->user()->first_name);
        return view('backend.admin.deshboard.admin_deshboard', compact('admin'));
    }
    public function adminLogout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login.form');
    }
}
