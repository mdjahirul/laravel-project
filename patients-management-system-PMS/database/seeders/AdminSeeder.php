<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create(
            [
                'first_name'=> 'Momen',
                'last_name'=> 'Shekh',
                'email'=> 'm@gmail.com',
                'phone'=> '01786356432',
                'password'=> '$2y$10$NKU47ZBsecdxqVf74BUWJuhV6xiXnjPaUy7yLpYfavtBzrljB56Qq',
                'confirm_password'=> '$2y$10$NKU47ZBsecdxqVf74BUWJuhV6xiXnjPaUy7yLpYfavtBzrljB56Qq',
                'gender'=> 'male',
                'role_id'=> 1,           
                'doctor_category'=> "null"           
            ]
        );
    }
}
