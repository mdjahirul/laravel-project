<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Admin extends Authenticatable
{
    use HasFactory;
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'password',
        'confirm_password',
        'doctor_category',
        'gender',
        'image',
        'role_id',
        'description'
    ];

    const ADMIN = 1;
    const DOCTOR = 2;
    const TEST_ADMIN = 3;
    const USER = 4;


    public function isAdmin(){
        return auth('admin')->user()->role_id == self::ADMIN ;
    }

    public function isDoctor(){
        return auth()->user()->role_id == self::DOCTOR;
    }
    public function isTestAdmin(){
        return auth()->user()->role_id == self::TEST_ADMIN;
    }
    public function isUser(){
        return auth('admin')->user()->role_id == self::USER;
    }


    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function adminRole()
    {
        return $this->belongsTo(Role::class, 'role_id','id');
    }
    
    public function doctorCategory()
    {
        return $this->belongsTo(DoctorCategory::class, 'doctor_category','id');
    }
}
