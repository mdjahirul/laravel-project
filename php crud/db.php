<?php
$sarvername = "localhost";
$username = "root";
$password = "";
$dbname = "php_crud";

try {
    $pdo = new PDO(
        "mysql:host=$sarvername; 
        dbname=$dbname",
        $username,
        $password
    );
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $pdo;

} catch (PDOException $e) {
  echo  $e->getMessage(). $e->getLine();
}
?>