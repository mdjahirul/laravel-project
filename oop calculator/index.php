<?php include('./calculator.php') ?>
<?php
$calculate = new Calculator;

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    $numberone = $_POST['number1'] ?? 0;
    $numbertwo = $_POST['number2'] ?? 0;
    $operator = $_POST['operator'];
}
switch ($operator) {
    case 'addation':
        $output = $calculate->addition($numberone, $numbertwo);
        break;
    case 'subtraction':
        $output = $calculate->subtraction($numberone, $numbertwo);
        break;
    case 'multiplication':
        $output = $calculate->multiplication($numberone, $numbertwo);
        break;
    case 'modulo':
        $output = $calculate->modulo($numberone, $numbertwo);
        break;
    default;
}

?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-3   position-absolute top-50 start-50 translate-middle">
                <div class="card">
                    <div class="card-header bg-primary text-white"> Save Now</div>
                    <div class="card-body">
                        <form action="" method="POST">
                            <div class="form-group">
                                <label for="number1"> Number1</label>
                                <input class="form-control" name="number1" type="number">
                            </div>
                            <div class="form-group">
                                <label for="number2"> Number2</label>
                                <input name="number2" class="form-control" type="number2">
                            </div>

                            <div class="form-group">

                                <label for="password">Oprator :</label>
                                <select class="form-select" name="operator" aria-label="Default select example">
                                    <option value="addation">Addation: + </option>
                                    <option value="subtraction">Subtraction: - </option>
                                    <option value="multiplication">Multiplication: * </option>
                                    <option value="modulo">Modulo: / </option>
                                </select>

                            </div>
                            <br>

                            <div>
                                <h4>
                                    Result: <?= $output ?? '' ?>
                                </h4>

                            </div>

                            <br>
                            <div>
                                <button type="submit" class="btn btn-secondary"> Calculat</button>


                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>


    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
</body>

</html>