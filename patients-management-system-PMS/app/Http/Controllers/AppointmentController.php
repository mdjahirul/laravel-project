<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Exception;
use App\Models\Appointment;
use Illuminate\Http\Request;
use App\Models\DoctorCategory;
use Carbon\Carbon;

class AppointmentController extends Controller
{

    public function index()
    {
        $appointments = Appointment::all();

        return view('appointment.appointment_list', compact('appointments'));
    }

    public function appointment_show($id)
    {
        $appointment = Appointment::find($id);
        return view('appointment.show_appointment', compact('appointment'));
    }

    public function appointment($id)
    {
        $doctor = Admin::findOrFail($id);
        return view('appointment.appointment_form', compact('doctor'));
    }

    public function store(Request $request)
    {

        $request->validate([
            'patient_name' => 'required',
            'doctor_name' => 'required',
            'doctor_category' => 'required',
            'age' => 'required|numeric',
            'phone' => 'required|numeric',
            'gender' => 'required',
            'appointment_date' => 'required',
            'address' => 'required',
            'description' => 'required',

        ]);




        try {


            $appointment = Appointment::create([

                'doctor_id' => $request->doctor_id,
                'patient_name' => $request->patient_name,
                'doctor_name' => $request->doctor_name,
                'doctor_category' => $request->doctor_category,
                'age' => $request->age,
                'phone' => $request->phone,
                'gender' => $request->gender,
                'appointment_date' => $request->appointment_date,
                'address' => $request->address,
                'description' => $request->description,

            ]);
            // dd($request->all());
            return redirect()->route('user.deshboard')->with('success', "Appointment Successfully");
        } catch (Exception $e) {
            dd($e->getMessage());
            return redirect()->back($e->getMessage());
        }
    }


    public function today_appointment()
    {

        $appointment_today = Appointment::where('doctor_id', auth('admin')->user()->id)->whereDate('created_at', Carbon::today())->get();

        return view('appointment.today_appointment', compact('appointment_today'));
    }


    public function show($id)
    {
        $prescription = Appointment::find($id);

        return view('backend.admin.prescription.show_prescription', ['prescription' => $prescription]);
    }
}
