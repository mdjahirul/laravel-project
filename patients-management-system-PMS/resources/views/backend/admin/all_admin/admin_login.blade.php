<x-backend.layout.master>


    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://kit.fontawesome.com/010be078f0.js" crossorigin="anonymous"></script>


    <!-- content Start -->
    <div class="container">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('message'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>{{ session('message') }}!</strong>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('success') }}!</strong> <button type="button" class="btn-close"
                    data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        {{-- <form action="{{ route('admin.login') }}" method="post">
            @csrf
            <div class="mb-3">
                <label for="email" class="form-label">Email </label>
                <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
            </div>
            <div class="mb-3">
                <label for="password" class="form-label">Password</label>
                <input type="password" name="password" class="form-control" id="password">
            </div>

            <div class="mb-3 form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Check me out</label>
            </div>
            <button type="submit" class="btn btn-primary">Login</button>
        </form> --}}


        <body class="bg-purple: #6f42c1;" style="background-color:#232e5c; ">
            <div id="login" >
                

                <div class="container" >
                    <div id="login-row" class="row justify-content-center align-items-center">
                        <div id="login-column" class="col-md-6">
                            <div id="login-box" class="col-md-12">

                                <form id="login-form" class="form" action="{{ route('admin.login') }}" method="post" >
                                    @csrf

                                  <div class="container p-4 rounded-2" style="background-color:#151b33a8;">

                                    <h3 class="text-center text-black pt-5"><i class="fa-solid fa-key " style='font-size:48px;color:rgba(12, 211, 211, 0.863)'></i></h3>

                                    <h3 class="text-center text-info">Admin Login</h3>
                                    <div class="form-group">
                                        <label for="email" class="text-info">Email:</label><br>
                                        <input type="email" name="email" id="email" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="text-info">Password:</label><br>
                                        <input type="password" name="password" id="password" class="form-control ">
                                    </div>
                                    <div class="form-group mt-5">
                                        
                                        <input type="submit" name="submit" class="btn btn btn-outline-info btn-md" value="submit">
                                    </div>
                                    
                                  </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </body>


    </div>

    <!-- content End -->

</x-backend.layout.master>
