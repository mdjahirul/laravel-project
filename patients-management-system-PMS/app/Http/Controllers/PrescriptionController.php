<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use Exception;

use App\Models\Prescription;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PrescriptionController extends Controller
{



public function prescription_list(){

    if(request()->keyword){

        $prescription = Prescription::where('patient_name','like', '%' . request()->keyword. '%' )->get();
        return view('backend.admin.prescription.prescription_list', compact('prescription'));

    }else{
        $prescription= Prescription::all();

        return view('backend.admin.prescription.prescription_list',compact('prescription'));
    }

   
}

    public function patients_prescription($id){
        
        $patient =Appointment::find($id);
        
        return view('backend.admin.prescription.prescription',compact('patient'));
    }


    public function store(Request $request){


// dd($request->all());
        $request->validate([
            'patient_name'=>'required',
            'doctor_name'=>'required',
            'doctor_category'=>'required',
            'medicine'=>'required',
            'age'=>'required|numeric',
            'phone'=>'required|numeric',
            'test'=>'required',
            'disease'=>'required',
            'gender'=>'required',
            'next_appointment'=>'required',
            'message'=>'required',
        ]);

        
        try {
            
            
            $prescription = Prescription::create([
                'patient_name'=>$request->patient_name,
                'doctor_name'=>$request->doctor_name,
                'doctor_category'=>$request->doctor_category,
                'medicine'=>$request->medicine,
                'age'=>$request->age,
                'phone'=>$request->phone,
                'test'=>$request->test,
                'disease'=>$request->disease,
                'gender'=>$request->gender,
                'next_appointment'=>$request->next_appointment,
                'message'=>$request->message,
                
            ]);
            // dd($request->all());
            return redirect()->route('patient.all')->with('success',"Prescription Create Successfully");
        } catch (Exception $e) {
            return redirect()->back($e->getMessage());
        }
    }

    public function show($id){
        $prescription= Prescription::find($id);

        return view('backend.admin.prescription.show_prescription',['prescription'=>$prescription]);
    }
}
