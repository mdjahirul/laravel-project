<x-backend.layouts.master>
    <x-slot name="page_title">
        Products
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Products
            </x-slot>
            <x-slot name="add">
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Products</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
<div class="card mb-4">
    <div class="card-header ">
        <div class="d-flex justify-content-between">
            <span><i class="fas fa-table me-1"></i>Products</span>
                <span>
                    <a class="btn btn-sm btn-primary text-left" href="{{ Route('products.create') }}" role="button">Add</a>
                </span>
        </div>
    </div>
    <div class="card-body">
        <x-backend.layouts.elements.message :message="session('message')"/>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>SL#</th>
                    <th>Name</th>
                    <th>Category Name</th>
                    <th>Brand Name</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $sl = 0;
                @endphp
                @foreach ($products as $product)
                    <tr>
                        <td>{{ ++$sl }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->category_name }}</td>
                        <td>{{ $product->brand_name }}</td>
                        <td>{{ $product->description }}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="{{ route('products.show', ['product'=>$product->id]) }}" role="button">Show</a>
                            <a class="btn btn-sm btn-warning" href="{{ route('products.edit', ['product'=>$product->id]) }}" role="button">Edit</a>
                            <form style="display: inline;" action="{{ route('products.destroy', ['product'=>$product->id]) }}" method="POST">
                                @csrf
                                @method('delete')
                                <button onclick="return confirm('Are you sure want to delete ?')" class="btn btn-sm btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

</x-backend.layouts.master>



