<x-backend.layout.master>

    <!-- Sidebar Start -->
    <x-backend.partials.sidebar />
    <!-- Sidebar End -->



    <!-- Content Start -->
    <div class="content">
        <!-- Navbar Start -->
        <x-backend.partials.navbar />
        <!-- Navbar End -->

        <!-- content Start -->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('message'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>{{ session('message') }}!</strong>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('success') }}!</strong> <button type="button" class="btn-close"
                    data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif



        <div class="container mt-5 mx-4">

            <table class="table table table-striped">
                <thead class="table-dark">
                    <tr class="text-center">
                        <th scope="col">Ser No</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Doctor id</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>

                    @php
                        
                    @endphp

                    @foreach ($patients as $key => $patient)
                        <tr class="text-center">
                            <td scope="row">{{ $key + 1 }}</td>
                            <td>{{ $patient->name }}</td>
                            <td>{{ $patient->email }}</td>
                            <td>{{ $patient->doctor_id }}</td>
                            <td>{{ $patient->phone }}</td>
                            <td>
                                <a class="btn btn-warning" href="{{ route('patient.show', $patient->id) }}">Show</a>
                                <a class="btn btn-primary"
                                    href="{{ route('patient.prescription', $patient->id) }}">Prescription</a>
                                {{-- <a class="btn btn-primary" href="{{ route('admin.doctor.prescription',$admin->id) }}">Edit</a>
                        <a class="btn btn-danger" href="{{ route('admin.doctors.prescription.delete',$admin->id) }}">Delete</a> --}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <!-- content End -->


        <!-- Footer Start -->
        <x-backend.partials.footer />
        <!-- Footer End -->

    </div>
    <!-- Content End -->

    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>



    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>

</x-backend.layout.master>
