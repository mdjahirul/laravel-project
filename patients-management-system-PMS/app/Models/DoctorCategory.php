<?php

namespace App\Models;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DoctorCategory extends Model
{
    use HasFactory;
    protected $fillable=[
        'name',
    ];

    public function doctor()
    {
       return $this->hasMany(Admin::class, 'doctor_id','id');
    }
}
