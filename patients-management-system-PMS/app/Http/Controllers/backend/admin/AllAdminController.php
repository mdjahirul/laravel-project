<?php

namespace App\Http\Controllers\backend\admin;

use Exception;
use App\Models\Role;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AllAdminController extends Controller
{


    public function allAdmins()
    {

        if (request()->keyword) {

            $admins = Admin::where('first_name', 'like', '%' . request()->keyword . '%')->get();
            return view('backend.admin.all_admin.index', compact('admins'));
        } else {
            $admins = Admin::all();
            return view('backend.admin.all_admin.index', ['admins' => $admins]);
        }
    }

    public function showAdmin($id)
    {
        $admin = Admin::find($id);
        return view('backend.admin.all_admin.show', ['admin' => $admin]);
    }

    public function editAdmin($id)
    {
        $roles = Role::all();
        $admin = Admin::find($id);
        return view('backend.admin.all_admin.edit', ['admin' => $admin, 'roles' => $roles]);
    }

    public function updateAdmin(Request $request, $id)
    {
        $admin = Admin::find($id);
        try {
            $request->validate([
                'admin_id' => 'required'
            ]);
            $admin = Admin::where('id', $id)->update([
                'role_id' => $request->admin_id
            ]);
            return redirect()->route('admin.all')->with('success', "Role Update successfully");
        } catch (Exception $e) {
            return redirect()->back($e->getMessage());
        }
    }

    public function adminDelete($id)
    {

        $user = Admin::find($id);
        $user->delete();
        return redirect()->route('admin.all')->with('success', "Role Deleted successfully");
    }


    public function Profile()
    {

        return view('backend.admin.all_admin.profile');
    }
}
